var form = document.getElementById("formConditional");
var jawaban = document.getElementById("jawaban");

form.addEventListener("submit", function (e) {
  e.preventDefault();

  /* 
        ATURAN PERMAINAN 
        // Output untuk Input nama = '' dan peran = ''
        // => "Nama harus diisi!"
 
        //Output untuk input jika nama != '' dan peran = ''
        // => "Halo Mikael, Pilih peranmu untuk memulai game!"
 
        //Output untuk Input jika nama != '' dan peran 'penyihir'
        // => "Selamat datang di Dunia Werewolf, Nina"
        // => "Halo Penyihir Nina, kamu dapat melihat siapa yang menjadi Werewolf!"
 
        //Output untuk Input jika nama != '' dan peran 'Werewolf'
        // => "Selamat datang di Dunia Werewolf, Danu"
        // "Halo Werewolf Danu, kamu dapat memilih siapa yang akan kamu makan malam ini!"
 
        //Output untuk Input jika nama != '' dan peran 'Rakjel'
        // => "Selamat datang di Dunia Werewolf, Zero"
        // => "Halo Rakjel Zero, Kamu menjadi rakyat jelata "
 
        PETUNJUK MENGERJAKAN
        1. Buat Kondisional agar output dari konsol sesuai yang diharapkan
        2. masukkan lah jawaban dari kondisi yang diberikan oleh User ke dalam variabel jawabanKonsol
    */

  // Tuliskan Code kamu di sini

  var name = document.getElementById("name").value.toLowerCase();
  var role = document.getElementById("role").value.toLowerCase();
  name = name.toLowerCase();
  role = role.toLowerCase();

  // buatlah kondisional berdasarkan dua variabel di atas yaitu name dan role, ketentuannya dapat dibaca di atas

  var output = "";
  if (name == "" && role == "") {
    output += "nama harus diisi! ";
  } else if (name != "" && role == "") {
    output += "Halo " + name + " , Pilih peranmu untuk memulai game!";
  } else if (name != "" && role == "penyihir") {
    output += "Selamat datang di Dunia Werewolf ," + name + ". <br> " + role + " " + name + " , kamu dapat melihat siapa yang menjadi Werewolf!";
  } else if (name != "" && role == "werewolf") {
    output += "Selamat datang di Dunia " + role + " " + name + ". <br>" + "Halo, Kamu dapat memilih siapa yang akan kamu makan malam ini!";
  } else if (name != "" && role == "rakjel") {
    output += "Selamat datang di Dunia Werewolf ," + name + ". <br>" + "Halo " + role + " " + name + " , Kamu menjadi rakyat jelata ";
  }

  var jawabanKonsol = output; // jawaban dari kondisional di-assign di sini
  // Code Sampai sini

  jawaban.innerHTML = jawabanKonsol;
});
