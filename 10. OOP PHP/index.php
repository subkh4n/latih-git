<?php
require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

  $sheep = new Animal("shaun");

  echo "Nama Hewan :". $sheep->name."<br>";
  echo "jumlah Lengan :".$sheep->legs."<br>";
  echo "berdarah dingin : " .$sheep->cold_blooded."<br> <br>";

  $ape = new Ape("Kera Sakti");

  echo "Nama Hewan :". $ape->name."<br>";
  echo "jumlah Lengan :".$ape->legs."<br>";
  echo "berdarah dingin : " .$ape->cold_blooded."<br> <br>";
  echo $ape->yell();


  $frog = new Frog("buduk");

  echo "Nama Hewan :". $frog->name."<br>";
  echo "jumlah Lengan :".$frog->legs."<br>";
  echo "berdarah dingin : " .$frog->cold_blooded."<br> <br>";
  echo $frog->jump();
?>